import unittest
import add_numbers

class unitTest_add_numbers(unittest.TestCase):
  """testing add_numbers"""
  
  def test_add_numbers(self):
    self.assertEquals(10,add_numbers.add_numbers(5,5),"adding 5 and 5")
    self.assertEquals(12,add_numbers.add_numbers(6,6),"wonder what happens when we fail?")

if __name__ == '__main__':
  unittest.main()
